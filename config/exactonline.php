<?php

return [
    /**
     * This product id is used when an item in a inquiry list or
     * order is not available in Exact Online. Then we will connect
     * it to this product id so we will be able to create the inquiry,
     * order etc.
     */
    'dummy_product_id' => 'a7fc0914-5f20-4ce3-bbb1-1c973bf40396',

    /**
     * How should we prefix when
     */
    'prefix' => [
        'inquiry' => 10,
    ],

    'visible_user_fields' => [
        'CustomerName',
        'Email',
        'FullName',
        'Phone',
        'UserName',
        'UserID',
        // 'BirthDate',
        // 'BirthName',
        // 'Created',
        // 'Creator',
        // 'CreatorFullName',
        // 'Customer',
        // 'EndDate',
        // 'FirstName',
        // 'Gender',
        // 'HasRegisteredForTwoStepVerification',
        // 'HasTwoStepVerification',
        // 'Initials',
        // 'IsAnonymised',
        // 'Language',
        // 'LastLogin',
        // 'LastName',
        // 'MiddleName',
        // 'Mobile',
        // 'Modified',
        // 'Modifier',
        // 'ModifierFullName',
        // 'Nationality',
        // 'Notes',
        // 'PhoneExtension',
        // 'ProfileCode',
        // 'StartDate',
        // 'StartDivision',
        // 'Title',
        // 'UserRoles',
        // 'UserRolesPerDivision',
        // 'UserTypesList',
    ]
];