<?php

namespace Marshmallow\Accounting\ExactOnline;

use Exception;
use Carbon\Carbon;
use Picqer\Financials\Exact\Item;
use Picqer\Financials\Exact\User;
use Picqer\Financials\Exact\Account;
use Picqer\Financials\Exact\Connection;
use Marshmallow\Ecommerce\Cart\Models\Inquiry;
use Marshmallow\Ecommerce\Cart\Models\Prospect;
use Marshmallow\Accounting\Accountable\Classes\AccountableModel;
use Marshmallow\Accounting\Accountable\Http\Resources\MeResource;
use Marshmallow\Accounting\Accountable\Models\AccountableConnect;
use Marshmallow\Accounting\Accountable\Contracts\AccountableInterface;
use Marshmallow\Accounting\Accountable\Http\Resources\AuthenticateResource;
use Marshmallow\Accounting\Accountable\Http\Resources\CreateInquiryResource;
use Marshmallow\Accounting\Accountable\Http\Resources\CreateProspectResource;

class ExactOnline implements AccountableInterface
{
	protected $connection;

	protected function setup ()
    {
    	$setup = new Connection;
        $setup->setRedirectUrl(env('EXACT_ONLINE_REDIRECT_URI'));
        $setup->setExactClientId(env('EXACT_ONLINE_CLIENT_ID'));
        $setup->setExactClientSecret(env('EXACT_ONLINE_CLIENT_SECRET'));
        return $setup;
    }

    public function authCallback ($auth_code)
    {
    	$this->connection = $this->setup();
        $this->connection->setAuthorizationCode($auth_code);
        $this->connection->connect();

        AccountableConnect::storeToken($this);

        return response()->json([
            'access_token' => $this->getAccessToken(),
        ]);
    }

    public function getAuthUrl (): AuthenticateResource
    {
    	return new AuthenticateResource(
    		$this->setup()->getAuthUrl()
    	);
    }

    protected function getConnection ()
    {
    	if ($this->connection) {
    		return $this->connection;
    	}

    	$connection_auth = AccountableConnect::getToken($this);
        if (!$connection_auth) {
        	throw new Exception('No tokens available available');
        }

        $this->connection = $this->setup();
        $this->connection->setAccessToken($connection_auth->access_token);
        $this->connection->setRefreshToken($connection_auth->refresh_token);
        $this->connection->setTokenExpires(Carbon::parse($connection_auth->token_expires)->timestamp);
        $this->connection->connect();

        AccountableConnect::storeToken($this);

        return $this->connection;
    }

    public function me (): MeResource
    {
        $me = new User(
        	$this->getConnection()
        );

        if (!$me) {
            throw new Exception("User not found", 1000);
        }
        if (!$me->get()) {
            throw new Exception("User not found", 1001);
        }
        if (!isset($me->get()[0])) {
            throw new Exception("User not found", 1002);
        }

        $user = $me->get()[0];

        return new MeResource($user);
    }

    public function getAccessToken (): string
    {
    	return $this->getConnection()->getAccessToken();
    }

    public function getRefreshToken (): string
    {
    	return $this->getConnection()->getRefreshToken();
    }

    public function getTokenExpires (): Carbon
    {
    	return Carbon::parse($this->getConnection()->getTokenExpires());
    }

    public function getProduct ($product_accountable_id): AccountableModel
    {
        return new AccountableModel(
        	$product_accountable_id,
        	(new Item($this->getConnection()))->find($product_accountable_id)
        );
    }

    public function createProspect (Prospect $prospect): CreateProspectResource
    {
        /**
         * Maak de prospect aan
         */
        $account = new Account($this->getConnection());
        $account->IsSales = 'true';
        $account->Status = 'P';
        $account->AddressLine1 = $prospect->address;
        $account->Name = $prospect->company_name;
        $account->Email = $prospect->email;
        $account->Phone = $prospect->phone_number;

        if ($prospect->country) {
            $account->Country = $prospect->country->alpha2;
        }
        
        $account = $account->save();
        
        /**
         * Maak een contact aan die we koppelen aan de prospect
         */
        $contact = new \Picqer\Financials\Exact\Contact($this->getConnection());
        $contact->Account = $account->ID;
        $contact->FirstName = $prospect->first_name;
        $contact->LastName = $prospect->last_name;
        $contact->Email = $prospect->email;
        $contact->Phone = $prospect->phone_number;
        $contact->save();

        return new CreateProspectResource(
            $account->ID,
            $account,
            $prospect
        );
    }

    public function createInquiry (Inquiry $inquiry): CreateInquiryResource
    {
        try {
            $quotation = new \Picqer\Financials\Exact\Quotation(
               $this->getConnection()
            );

            $account = $inquiry->getCustomerOrProspect();
            if (!$account->accountable) {
                $account->syncToAccounting();
            }

            $quotation->DeliveryAccount = $account->accountable->accounting_id;
            $quotation->Description = $inquiry->note;
            $quotation->OrderAccount = $account->accountable->accounting_id;
            $quotation->YourRef = config('exactonline.prefix.inquiry') . $inquiry->id;

            foreach ($inquiry->items as $item) {

                $accountable = $item->product->accountable;
                if ($accountable) {
                    $accounting_id = $accountable->accounting_id;
                } else {
                    $accounting_id = config('exactonline.dummy_product_id');
                }

                if (!$accounting_id) {
                    throw new Exception("Could not create inquiry because we have a product without accounting id. Please create this product in your accounting software or set up a `dummy` product in your config file.");
                }

                $quotation->addItem([
                    'Item' => $accounting_id,
                    'Quantity' => $item['quantity'],
                ]);
            }

        
            $quotation = $quotation->save();

            return new CreateInquiryResource(
                $quotation->QuotationID,
                $quotation,
                $inquiry
            );

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 1);
        }
    }
}
